====================================
Build Instructions for the GIS Stack
====================================

A GIS stack will be comprised of three servers behind a public-facing load
balancer.

        1. **Web GIS** - Runs the ArcGIS Web Adaptor and hosts web
           applications.
        2. **Application GIS** - The ArcGIS server.
        3. **Database GIS** - Runs PostgreSQL with our geodatabase.

The stack will be replicated as three separate layers: *dev*, *test*, and
*prod*.

These servers need to follow naming conventions. For example, on the new *dev*
layer the machines should be named ``devwebgis01``, ``devappgis01``, and
``devdbgis01``.

Installation
============
Please be sure to read the entire README before executing the bootstrap
scripts. The following steps are the *short version*, and omit details such as
ec2 instance requirements and the necessary changes that need to be made to
the scripts' JSON configuration files.

Installation Overview
---------------------
1. Bootstrap the database gis machine.
2. License and bootstrap the application gis machine.
3. Postinstall the database gis machine.
4. Bootstrap the web gis machine.

1. Bootstrap the database gis machine
-------------------------------------
This operation requires that you can establish a secure shell connection to the
**database GIS** machine as the ``ubuntu`` user.

Execute the ``bootstrap_dbgis.py`` script.

.. code-block:: bash
   
        $ ./bootstrap_dbgis.py

There's another script that needs to be executed, but only after an enterprise
geodatabase set up on the database server. This can only occur after the
application gis machine has been licensed and bootstrapped.

2. License and bootstrap the application gis machine
----------------------------------------------------
This operation requires that you can establish a secure shell connection to the
**application GIS** machine as the ``arcgis`` user.

Execute the ``bootstrap_appgis_license.py`` and ``bootstrap_appgis.py`` scripts.

.. code-block:: bash
   
        $ ./bootstrap_appgis_license.py && ./bootstrap_appgis.py


3. Postinstall the database gis machine
---------------------------------------
This operation requires that you can establish a secure shell connection to the
**database GIS** machine as the ``ubuntu`` user.

Execute the ``bootstrap_dbgis_postinstall.py`` script.

.. code-block:: bash
   
        $ ./bootstrap_dbgis_postinstall.py


4. Bootstrap the web gis machine
--------------------------------
For now, manually install the ArcGIS Web Adaptor.

It can be downloaded from here:
https://s3.amazonaws.com/gisstack/Web_Adaptor_Java_Linux_103_142169.gz

Installation instructions can be found here:
http://server.arcgis.com/en/server/latest/install/linux/installing-the-arcgis-web-adaptor-server-.htm

*Work in progress.*

Instance Requirements
=====================
These requirements are not set in stone, but represent a best effort to define
the needs of the RFSpot GIS environment.

Web GIS
-------
- The machine will use the Ubuntu Server 14.04 LTS (HVM), SSD Volume Type
  (``ami-d05e75b8``).
- It will use Apache Tomcat 7 as a web server.
- The security group will allow TCP traffic over ports ``22``, ``80`` and
  ``443``, and *may* allow exposure to the public (undecided).
- *This machine is still being specified*.

Application GIS
---------------
- The machine will use the pre-baked AMI provided by esri (``ami-7649c71e``).
- Based on some restrictions imposed by the esri AMI, we will use the
  ``c3.2xlarge`` instance type.
- It will have an attached EBS volume of 500 GB, and the mount point should be
  ``/mnt/data``.
- The security group will allow TCP traffic over ports ``22``, ``6080``, 
  ``6443``, ``6143``, ``6180``, ``5565``, ``5575``, ``2181``, ``27271``,
  ``27272``, and ``27273``, but no exposure to the public. We may need to add
  or remove some ports from this list in the future.

+-------+-------------------------------------------------------------------+
| Port  | Description                                                       |
+=======+===================================================================+
| 6080  | ArcGIS Server communicates through port 6080                      |
+-------+-------------------------------------------------------------------+
| 6443  | When SSL is enabled, ArcGIS Server uses port 6443                 |
+-------+-------------------------------------------------------------------+
| 6143  | ArcGIS GeoEvent Manager and administrator REST endpoints          |
+-------+-------------------------------------------------------------------+
| 6180  | GeoEvent Extension REST endpoints                                 |
+-------+-------------------------------------------------------------------+
| 5565  | Default Receive Text from a TCP Socket Input Connector port       |
+-------+-------------------------------------------------------------------+
| 5575  | Default Push Text to an External TCP Socket Output Connector port |
+-------+-------------------------------------------------------------------+
| 2181  | ZooKeeper, which is responsible for persisting GeoEvent Extension |
|       | configurations among machines in the cluster                      |
+-------+-------------------------------------------------------------------+
| 27271 | Ports used by RabbitMQ                                            |
+-------+                                                                   |
| 27272 |                                                                   |
+-------+                                                                   |
| 27273 |                                                                   |
+-------+-------------------------------------------------------------------+

Database GIS
------------
- The machine will use the Ubuntu Server 14.04 LTS (HVM), SSD Volume Type
  (``ami-d05e75b8``).
- It will be a storage optimized ec2 instance, ``d2.2xlarge``.
- EBS volumes:

  1. For Operating system and software: One 256 GB Provisioned IOPS SSD.
  2. For Data: Five 1 TB Provisioned IOPS SSD.

- Software RAID 0 configuration for the data drives is recommended, and the
  mount point should be ``/mnt/data``.
- The security group will allow TCP traffic over ports ``22`` and ``5432``, but
  no exposure to the public.
- ``pg_hba.conf`` and ``postgresql.conf`` need to allow remote connections.

Example ``pg_hba.conf``:

.. code-block::
        
        # This file was automatically generated and dropped off by Chef!

        # PostgreSQL Client Authentication Configuration File
        # ===================================================
        #
        # Refer to the "Client Authentication" section in the PostgreSQL
        # documentation for a complete description of this file.

        # TYPE  DATABASE        USER            ADDRESS                 METHOD

        ###########
        # Other authentication configurations taken from chef node defaults:
        ###########

        local   all             postgres                                ident

        local   all             all                                     ident

        host    all             all             127.0.0.1/32            md5

        host    all             all             ::1/128                 md5

        # "local" is for Unix domain socket connections only
        local   all             all                                     peer
        
        # The line below was added for this example
        host    all             all             0.0.0.0/0               md5

Example ``postgresql.conf``:

.. code-block::

        # PostgreSQL configuration file
        # This file was automatically generated and dropped off by chef!
        # Please refer to the PostgreSQL documentation for details on
        # configuration settings.

        data_directory = '/var/lib/postgresql/9.3/main'
        datestyle = 'iso, mdy'
        default_text_search_config = 'pg_catalog.english'
        external_pid_file = '/var/run/postgresql/9.3-main.pid'
        hba_file = '/etc/postgresql/9.3/main/pg_hba.conf'
        ident_file = '/etc/postgresql/9.3/main/pg_ident.conf'

        # listen_addresses set to '*' for this example
        listen_addresses = '*'
        
        log_line_prefix = '%t '
        max_connections = 100
        port = 5432
        shared_buffers = '24MB'
        ssl = on
        ssl_cert_file = '/etc/ssl/certs/ssl-cert-snakeoil.pem'
        ssl_key_file = '/etc/ssl/private/ssl-cert-snakeoil.key'
        unix_socket_directories = '/var/run/postgresql'


Configuration
=============
The bootstrap scripts rely on JSON configuration files. These need to be
tailored to fit the environment.

Within the ``services/build_server`` directory, we have the following
structure. It may change over time, but it should retain this basic
structure.

.. code-block::

        ├── README.rst
        ├── artifacts
        │   ├── libst_raster_pg.so
        │   └── st_geometry.so
        ├── bootstrap_appgis.py
        ├── bootstrap_dbgis.py
        ├── bootstrap_dbgis_postinstall.py
        ├── bootstrap_appgis_license.py
        ├── bootstrap_webgis.py
        ├── config
        │   ├── appgis.json    <-- application GIS configuration file
        │   ├── dbgis.json     <-- database GIS configuration file
        │   └── webgis.json    <-- web GIS configuration file
        ├── license
        │   └── ArcGISforServerAdvancedEnterprise_server_248781.prvc
        ├── logs
        ├── rfsgisstack
        │   ├── __init__.py
        │   ├── appgis
        │   │   └── __init__.py
        │   ├── dbgis
        │   │   └── __init__.py
        │   └── webgis
        │       └── __init__.py
        └── templates
            └── create_roles.sql

The ``appgis.json`` is of particular importance, because it actually has to
talk to the database GIS server instance during the execution of the
``bootstrap_appgis.py`` script. In the below example, the value for
``instance`` needs to be the network address for the database GIS server.

.. code-block:: json

        {
                "agsadmin":
                {
                        "username": "rfspot_admin",
                        "password": "Cardiff83"
                },
                "authorize":
                {
                        "utility": "/arcgis/server/tools/authorizeSoftware",
                        "email": "allan@rfspot.com",
                        "keycodes": "/arcgis/server/framework/runtime/.wine/drive_c/Program Files/ESRI/License10.3/sysgen/keycodes"
                },
                "db":
                {
                        "instance": "devdbgis01.rfspot.com",
                        "admin": "gisadmin",
                        "admin_pw": "RFSpot!gis",
                        "name": "gis01",
                        "tablespace": "gis_data"
                }
        }

The ``dbgis.json`` contains relevant information for bootstrapping the database GIS server.

.. code-block:: json

        {
                "admin":
                {
                        "user": "gisadmin",
                        "password": "RFSpot!gis"
                },
                "database": "gis01",
                "tablespace":
                {
                        "path": "/mnt/data/postgresql",
                        "name": "gis_data"
                },
                "pkglibdir": "/usr/lib/postgresql/9.3/lib"
        }

The ``webgis.json`` is still under development as of the writing of this document.

.. code-block:: json

        {
                "configuration": "goes here."
        }

