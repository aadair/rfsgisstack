#!/usr/bin/python
"""
Utility script for bootstrapping a RFSpot dbgis machine.

logging information is stored in the ``logs`` directory.

This script uses a JSON configuration file for input parameters. If you
wish to make changes, please edit the ``config/dbgis.json`` file.

"""
import logging
from os.path import dirname, join, realpath
from rfsgisstack import config, log_file, dbgis

if __name__ == '__main__':

    # get/create a logger
    logger = logging.getLogger(__file__)
    logger.setLevel(logging.INFO)

    # add file handler to logger
    logger.addHandler(log_file(__file__))

    # try to bootstrap the machine
    try:
        logger.info('Loading configuration ...')
        cfg = config(file_path=__file__, json_file='dbgis.json')

        logger.info('Creating template databases ...')
        dbgis.create_template_dbs()

        logger.info('Installing extensions ...')
        dbgis.install_extensions()

        logger.info('Enabling extensions ...')
        dbgis.enable_extensions()

        logger.info('Creating tablespace ...')
        dbgis.create_tablespace(pg_dir=cfg['tablespace']['path'],
                                pg_data='data',
                                tablespace=cfg['tablespace']['name'])

        logger.info('Creating enterprise geodatabase ...')
        stgeom_path = join(dirname(realpath(__file__)),
                           'artifacts/st_geometry.so')
        raster_path = join(dirname(realpath(__file__)),
                           'artifacts/libst_raster_pg.so')
        dbgis.create_enterprise_gdb(pkglibdir=cfg['pkglibdir'],
                                    stgeomlib=stgeom_path,
                                    rasterlib=raster_path,
                                    tablespace=cfg['tablespace']['name'],
                                    db_name=cfg['database'])

        logger.info('Creating superuser ...')
        dbgis.create_superuser(admin=cfg['admin']['user'],
                               admin_pw=cfg['admin']['password'])

        logger.info('Success.')

    except Exception as e:
        logger.critical(e)
