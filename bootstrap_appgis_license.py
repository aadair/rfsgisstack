#!/usr/bin/python
"""
Utility script for bootstrapping a RFSpot appgis machine.

logging information is stored in the ``logs`` directory.

This script uses a JSON configuration file for input parameters. If you
wish to make changes, please edit the ``config/appgis.json`` file.

"""
import logging
from os.path import dirname, join, realpath
from rfsgisstack import config, log_file
from subprocess import check_call

if __name__ == '__main__':

    # get/create a logger
    logger = logging.getLogger(__file__)
    logger.setLevel(logging.INFO)

    # add file handler to logger
    logger.addHandler(log_file(__file__))

    # try to bootstrap the machine
    try:
        logger.info('Loading configuration ...')
        cfg = config(file_path=__file__, json_file='appgis.json')

        # we need to authorize arcgis server before we can use arcpy
        logger.info('Authorizing ArcGIS server ...')
        lic = join(dirname(realpath(__file__)), 'license',
                   'ArcGISforServerAdvancedEnterprise_server_248781.prvc')
        check_call([cfg['authorize']['utility'], '-f', lic, '-e',
                    cfg['authorize']['email']])

        logger.info('Success.')

    except Exception as e:
        logger.critical(e)
