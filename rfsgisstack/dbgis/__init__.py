"""
Generic functions for bootstrapping a RFSpot dbgis machine.

"""
from os import sep
from os.path import basename
from subprocess import check_call


def create_template_dbs():
    """
    This function creates some template databases.

    +--------------------+------------------------------------------+
    | Template name      | Description                              |
    +====================+==========================================+
    | template_postgis   | Will have PostGIS enabled.               |
    +--------------------+------------------------------------------+
    | template_pgrouting | Will have PostGIS and pgRouting enabled. |
    +--------------------+------------------------------------------+

    At this point the databases are nothing special, they still need to have
    extensions installed and enabled.

    """
    check_call(['sudo', '-u', 'postgres', 'createdb', 'template_postgis',
                '-E', 'UTF-8'])
    check_call(['sudo', '-u', 'postgres', 'createdb', 'template_pgrouting',
                '-E', 'UTF-8'])


def install_extensions():
    """
    Installs PostGIS and pgRouting on the dbgis machine.

    In the future we may want to build from source instead.

    """
    check_call(['sudo', 'apt-get', '-qq', '--yes', '--force-yes', 'install',
                'postgis', 'postgis-doc', 'postgresql-9.3-postgis-scripts'])
    check_call(['sudo', 'apt-add-repository', '--yes',
                'ppa:ubuntugis/ppa'])
    check_call(['sudo', 'apt-add-repository', '--yes',
                'ppa:georepublic/pgrouting'])
    check_call(['sudo', 'apt-get', '-qq', 'update'])
    check_call(['sudo', 'apt-get', '-qq', '--yes', '--force-yes', 'install',
                'postgresql-9.3-pgrouting'])


def enable_extensions():
    """
    Enables PostGIS and pgRouting on template databases.

    """
    check_call(['sudo', '-u', 'postgres', 'psql', '-d', 'template_postgis',
                '-c', 'CREATE EXTENSION postgis;'])
    check_call(['sudo', '-u', 'postgres', 'psql', '-d', 'template_pgrouting',
                '-c', 'CREATE EXTENSION postgis;'])
    check_call(['sudo', '-u', 'postgres', 'psql', '-d', 'template_pgrouting',
                '-c', 'CREATE EXTENSION pgrouting;'])


def create_tablespace(pg_dir, pg_data, tablespace):
    """
    Creates and configures a tablespace.

    :param pg_dir: Directory to be owned by postgres user
    :param pg_data: Directory for the database
    :param tablespace: Name of tablespace

    """
    check_call(['sudo', 'mkdir', '{0}'.format(pg_dir)])
    check_call(['sudo', 'mkdir', '{0}{1}{2}'.format(pg_dir, sep, pg_data)])
    check_call(['sudo', 'chown', '-R', 'postgres:postgres',
                '{0}'.format(pg_dir)])
    check_call(['sudo', '-u', 'postgres', 'psql', '-c',
                'CREATE TABLESPACE {0} LOCATION \'{1}{2}{3}\';'.format(tablespace,
                                                                       pg_dir,
                                                                       sep,
                                                                       pg_data)])


def create_enterprise_gdb(pkglibdir, stgeomlib, rasterlib, tablespace,
                          db_name):
    """
    Creates database to host the enterprise geodatabase.

    :param pkglibdir: PostgreSQL's PKGLIBDIR
    :param stgeomlib: Path to local st_geometry.so
    :param rasterlib: Path to local libst_raster_pg.so
    :param tablespace: Name of tablepsace
    :param db_name: Database name

    """
    check_call(['sudo', '-u', 'postgres', 'createdb', '-D',
                '{0}'.format(tablespace), '-T', 'template_pgrouting',
                '{0}'.format(db_name), '-E', 'UTF-8'])
    check_call(['sudo', 'cp', '{0}'.format(stgeomlib),
                '{0}'.format(pkglibdir)])
    check_call(['sudo', 'cp', '{0}'.format(rasterlib),
                '{0}'.format(pkglibdir)])
    check_call(['sudo', 'chmod', '755', '{0}{1}{2}'.format(pkglibdir, sep,
                                                           basename(stgeomlib))])
    check_call(['sudo', 'chmod', '755', '{0}{1}{2}'.format(pkglibdir, sep,
                                                           basename(rasterlib))])


def create_superuser(admin, admin_pw):
    """
    Creates a superuser.

    :param admin: Administrative username to be created
    :param admin_pw: Administrative password to be created

    """
    check_call(['sudo', '-u', 'postgres', 'psql', '-c',
                'CREATE ROLE {0} ENCRYPTED PASSWORD \'{1}\' SUPERUSER CREATEDB CREATEROLE INHERIT LOGIN;'.format(admin, admin_pw)])


def create_roles(db_name, sql_file):
    """
    Creates roles from a SQL file against a database as the postgres user.

    :param db_name: Database name
    :param sql_file: Path to SQL file

    """
    with open(sql_file, 'r') as sqlf:
        sql_text = sqlf.read()

    # SQL file is preformatted to accept the database name
    sql_text.format(db_name)

    check_call(['sudo', '-u', 'postgres', 'psql', '-d', db_name, '-f',
                sql_text])
