"""
The rfsgisstack package was created for the purpose of aiding in the deployment
of RFSpot's GIS stack.

"""
__version__ = 1.0

import json
import logging
from os.path import basename, dirname, join, realpath


def log_file(file_path):
    """
    Returns a logging FileHandler tailored for the rfsgisstack package.

    :param file_path: Expected value is the __file__ attribute of the calling
                      bootstrap script. The calling script should be in the
                      parent directory of the rfsgisstack package.

    """
    # logging formatter
    formatter = logging.Formatter('%(asctime)s, %(name)s, '
                                  '%(levelname)s, %(message)s')

    # create a file handler and set level to INFO
    log_p = join(dirname(file_path), 'logs',
                 '{0}.log'.format(basename(file_path)))
    log_f = logging.FileHandler(log_p)
    log_f.setLevel(logging.INFO)
    log_f.setFormatter(formatter)

    return log_f


def config(file_path, json_file):
    """
    Returns a dictionary containing configuration information.

    :param file_path: Expected value is the __file__ attribute of the calling
                      bootstrap script. The calling script should be in the
                      parent directory of the rfsgisstack package.

    :param json_file: The filename of the JSON file.

    """
    json_path = join(dirname(realpath(file_path)), 'config', json_file)
    with open(json_path, 'r') as json_f:
        cfg = json.load(json_f)

    return cfg
