"""
Generic functions for bootstrapping a RFSpot appgis machine.

"""
from arcpy.management import CreateDatabaseConnection, \
    CreateEnterpriseGeodatabase, CreateSpatialType, CreateRasterType
import json
from os.path import join
from urllib import urlencode
from urllib2 import Request, urlopen


def create_site(ags_username, ags_password):
    """
    Creates an ArcGIS site.

    :param ags_username: New ArcGIS site administrator username
    :param ags_password: New ArcGIS site administrator password

    """
    # ArcGIS server REST admin endpoint
    restadmin = u'http://localhost:6080/arcgis/admin'

    # get default configuration
    request = u'createNewSite'
    endpt = u'/'.join((restadmin, request))
    params = {u'f': u'json'}
    data = urlopen(Request(url=endpt, data=urlencode(params)))
    default_cfg = json.loads(data.read())

    # add to the config
    default_cfg[u'f'] = u'json'
    default_cfg[u'username'] = ags_username
    default_cfg[u'password'] = ags_password
    default_cfg[u'runAsync'] = 'false'

    # create the site
    request = u'createNewSite'
    endpt = u'/'.join((restadmin, request))
    data = urlopen(Request(url=endpt, data=urlencode(default_cfg)))
    response = json.loads(data.read())
    status = response.get(u'status')
    if status and status == u'error':
        raise Exception(status)


def create_spatial_type(directory, instance, db_admin, db_admin_pw):
    """
    Creates esri spatial types on template databases.

    :param directory: Directory to hold sde connections
    :param instance: Network address of the database server
    :param db_admin: Database administrator username
    :param db_admin_pw: Database administrator password

    """
    CreateDatabaseConnection(out_folder_path=directory,
                             out_name='connection_postgis.sde',
                             database_platform='POSTGRESQL',
                             instance=instance,
                             account_authentication='DATABASE_AUTH',
                             username=db_admin,
                             password=db_admin_pw,
                             save_user_pass='SAVE_USERNAME',
                             database='template_postgis')

    CreateSpatialType(input_database=join(directory,
                                          'connection_postgis.sde'),
                      sde_user_password=db_admin_pw)

    CreateDatabaseConnection(out_folder_path=directory,
                             out_name='connection_pgrouting.sde',
                             database_platform='POSTGRESQL',
                             instance=instance,
                             account_authentication='DATABASE_AUTH',
                             username=db_admin,
                             password=db_admin_pw,
                             save_user_pass='SAVE_USERNAME',
                             database='template_pgrouting')

    CreateSpatialType(input_database=join(directory,
                                          'connection_pgrouting.sde'),
                      sde_user_password=db_admin_pw)


def create_enterprise_gdb(instance, db_admin, db_admin_pw, db_name,
                          tablespace, keycodes):
    """
    Creates an enterprise geodatabase.

    :param instance: Network address of the database server
    :param db_admin: Database administrator username
    :param db_admin_pw: Database administrator password
    :param db_name: Database name
    :param tablespace: Tablespace name
    :param keycodes: ArcGIS authorization codes

    """
    CreateEnterpriseGeodatabase(database_platform='POSTGRESQL',
                                instance_name=instance,
                                database_name=db_name,
                                database_admin=db_admin,
                                database_admin_password=db_admin_pw,
                                gdb_admin_name='sde',
                                gdb_admin_password=db_admin_pw,
                                tablespace_name=tablespace,
                                authorization_file=keycodes)


def create_sde_connection(directory, name, instance, user, password, db_name):
    """
    Creates a SDE connection file.

    :param directory: Output directory to hold the sde connection file
    :param name: Filename of the sde connection
    :param instance: Network address fo the database server
    :param user: Database username
    :param password: Database password
    :param db_name: Name of the database

    """
    CreateDatabaseConnection(out_folder_path=directory,
                             out_name=name,
                             database_platform='POSTGRESQL',
                             instance=instance,
                             account_authentication='DATABASE_AUTH',
                             username=user,
                             password=password,
                             save_user_pass='SAVE_USERNAME',
                             database=db_name)


def create_raster_type(sde_connection):
    """
    Creates raster type on an authorized geodatabase.

    :param sde_connection: SDE connection file

    """
    CreateRasterType(input_database=sde_connection)
