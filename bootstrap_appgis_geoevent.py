#!/usr/bin/python
"""
Utility script for bootstrapping the ArcGIS GeoEvent extension on a RFSpot
appgis machine.

logging information is stored in the ``logs`` directory.

This script uses a JSON configuration file for input parameters. If you
wish to make changes, please edit the ``config/webgis.json`` file.

"""
import logging
from rfsgisstack import config, log_file, appgis

if __name__ == '__main__':

    # get/create a logger
    logger = logging.getLogger(__file__)
    logger.setLevel(logging.INFO)

    # add file handler to logger
    logger.addHandler(log_file(__file__))

    # try to bootstrap the machine
    try:
        logger.info('Loading configuration ...')
        cfg = config(file_path=__file__, json_file='appgis.json')

        logger.info('Success.')

    except Exception as e:
        logger.critical(e)
