#!/usr/bin/python
"""
Post installation utility script for bootstrapping a RFSpot dbgis machine.

This should be run after the appgis machine has authorized a geodatabase on the
dbgis machine.

logging information is stored in the ``logs`` directory.

This script uses a JSON configuration file for input parameters. If you
wish to make changes, please edit the ``config/appgis.json`` file.

"""
import logging
from os.path import dirname, join, realpath
from rfsgisstack import config, dbgis, log_file

if __name__ == '__main__':

    # get/create a logger
    logger = logging.getLogger(__file__)
    logger.setLevel(logging.INFO)

    # add file handler to logger
    logger.addHandler(log_file(__file__))

    # try to bootstrap the machine
    try:
        logger.info('Loading configuration ...')
        cfg = config(file_path=__file__, json_file='dbgis.json')

        logger.info('Creating database roles ...')
        sql_path = join(dirname(realpath(__file__)),
                        'templates/create_roles.sql')
        dbgis.create_roles(db_name=cfg['database'], sql_file=sql_path)

        logger.info('Success.')

    except Exception as e:
        logger.critical(e)
