#!/arcgis/server/tools/python
"""
Utility script for bootstrapping a RFSpot appgis machine.

logging information is stored in the ``logs`` directory.

This script uses a JSON configuration file for input parameters. If you
wish to make changes, please edit the ``config/appgis.json`` file.

"""
import logging
from os.path import expanduser, join
from rfsgisstack import appgis, config, log_file

if __name__ == '__main__':

    # get/create a logger
    logger = logging.getLogger(__file__)
    logger.setLevel(logging.INFO)

    # add file handler to logger
    logger.addHandler(log_file(__file__))

    # try to bootstrap the machine
    try:
        logger.info('Loading configuration ...')
        cfg = config(file_path=__file__, json_file='appgis.json')

        logger.info('Creating ArcGIS site ...')
        appgis.create_site(ags_username=cfg['agsadmin']['username'],
                           ags_password=cfg['agsadmin']['password'])

        logger.info('Creating spatial type on database server ...')
        appgis.create_spatial_type(directory=expanduser('~'),
                                   instance=cfg['db']['instance'],
                                   db_admin=cfg['db']['admin'],
                                   db_admin_pw=cfg['db']['admin_pw'])

        logger.info('Creating enterprise geodatabase ...')
        appgis.create_enterprise_gdb(instance=cfg['db']['instance'],
                                     db_admin=cfg['db']['admin'],
                                     db_admin_pw=cfg['db']['admin_pw'],
                                     db_name=cfg['db']['name'],
                                     tablespace=cfg['db']['tablespace'],
                                     keycodes='Z:{0}'.format(cfg['authorize']
                                                                ['keycodes']))

        logger.info('Creating raster type on enterprise geodatabase ...')
        sdeconn_name = '{0}@{1}_{2}.sde'.format('sde',
                                                cfg['db']['name'],
                                                cfg['db']['instance'])
        appgis.create_sde_connection(directory=expanduser('~'),
                                     name=sdeconn_name,
                                     instance=cfg['db']['instance'],
                                     user='sde',
                                     password=cfg['db']['admin_pw'],
                                     db_name=cfg['db']['name'])
        appgis.create_raster_type(join(expanduser('~'), sdeconn_name))

        logger.info('Success.')

    except Exception as e:
        logger.critical(e)
