/*

create_roles.sql

*/

-- Create our custom roles
CREATE ROLE gis_data_creator
    WITH LOGIN ENCRYPTED PASSWORD 'data_creator_0!';
CREATE ROLE gis_data_editor
    WITH LOGIN ENCRYPTED PASSWORD 'data_editor_0!';
CREATE ROLE gis_data_viewer
    WITH LOGIN ENCRYPTED PASSWORD 'data_viewer_0!';

-- PostGIS stuff
GRANT SELECT, INSERT, UPDATE, DELETE 
    ON TABLE public.geometry_columns 
    TO sde, gis_data_creator;

GRANT SELECT
    ON TABLE public.spatial_ref_sys
    TO sde, gis_data_creator, gis_data_editor, gis_data_viewer;

-- Privileges for gis_data_creator
GRANT SELECT, INSERT, UPDATE, DELETE, TRUNCATE, REFERENCES, TRIGGER
    ON ALL TABLES IN SCHEMA public, sde
    TO gis_data_creator;
GRANT EXECUTE
    ON ALL FUNCTIONS IN SCHEMA public, sde
    TO gis_data_creator;
GRANT CREATE, CONNECT, TEMPORARY
    ON DATABASE {0}
    TO gis_data_creator;
GRANT USAGE
    ON SCHEMA public, sde
    TO gis_data_creator;
CREATE SCHEMA AUTHORIZATION gis_data_creator;

-- Privileges for gis_data_editor
GRANT SELECT, INSERT, UPDATE, DELETE, TRUNCATE, REFERENCES, TRIGGER
    ON ALL TABLES IN SCHEMA public, sde
    TO gis_data_editor;
GRANT EXECUTE
    ON ALL FUNCTIONS IN SCHEMA public, sde
    TO gis_data_editor;
GRANT CONNECT, TEMPORARY
    ON DATABASE {0}
    TO gis_data_editor;
GRANT USAGE
    ON SCHEMA public, sde
    TO gis_data_editor;

-- Privileges for gis_data_viewer
GRANT SELECT
    ON ALL TABLES IN SCHEMA public, sde
    TO gis_data_viewer;
GRANT CONNECT, TEMPORARY
    ON DATABASE {0}
    TO gis_data_viewer;
GRANT USAGE
    ON SCHEMA public, sde
    TO gis_data_viewer;

-- More roles
CREATE ROLE gis_data_editor_01
    WITH LOGIN ENCRYPTED PASSWORD 'data_editor_0!'
    IN ROLE gis_data_editor;
CREATE ROLE gis_data_editor_02
    WITH LOGIN ENCRYPTED PASSWORD 'data_editor_0@'
    IN ROLE gis_data_editor;
CREATE ROLE gis_data_editor_03
    WITH LOGIN ENCRYPTED PASSWORD 'data_editor_0#'
    IN ROLE gis_data_editor;
CREATE ROLE gis_data_viewer_01
    WITH LOGIN ENCRYPTED PASSWORD 'data_viewer_0!'
    IN ROLE gis_data_viewer;
CREATE ROLE gis_data_viewer_02
    WITH LOGIN ENCRYPTED PASSWORD 'data_viewer_0@'
    IN ROLE gis_data_viewer;
CREATE ROLE gis_data_viewer_03
    WITH LOGIN ENCRYPTED PASSWORD 'data_viewer_0#'
    IN ROLE gis_data_viewer;
CREATE ROLE westfield_beacon_data_editor_01
    WITH LOGIN ENCRYPTED PASSWORD 'westfield_editor_0!'
    IN ROLE gis_data_editor;
CREATE ROLE westfield_beacon_data_viewer_01
    WITH LOGIN ENCRYPTED PASSWORD 'westfield_viewer_01'
    IN ROLE gis_data_viewer;
